#include "lista.h"

float Compra::total = 0;

Lista::Lista() {
	bh = new Bebida(); 
	bt = new Bebida();
	em_comum_declarar(bh, bt);	
 
	fh = new Fruta();
	ft = new Fruta();
	em_comum_declarar(fh, ft);

	ch = new Cd();
	ct = new Cd();
	em_comum_declarar(ch, ct);

	lh = new Livro();
	lt = new Livro();
	em_comum_declarar(lh, lt);

	sh = new Salgado();
	st = new Salgado();
	em_comum_declarar(sh, st);

	dvh = new Dvd();
	dvt = new Dvd();
	em_comum_declarar(dvh, dvt);

	dh = new Doce();
	dt = new Doce();
	em_comum_declarar(dh, dt);

	comph = new Compra();
	compt = new Compra();
	
	comph->next = compt;
	compt->prev = comph;
	comph->prev = NULL;
	compt->next = NULL;

}

Lista::~Lista(){
 
}

//int get_quant(); 

void Lista::inserir_inicio_b(int c, string d, float p, int q, float t, float a, int val){
	Bebida *n = new Bebida(c, d, p, q, t, a, val);
	Bebida *h = bh;
	em_comum_inserir(h, n);


}


void Lista::inserir_inicio_f(int c, string d, float p, int q, int l, int da, int val){
	Fruta *n = new Fruta(c, d, p, q, l, da, val);
	Fruta *h = fh;
	em_comum_inserir(h, n);
}


void Lista::inserir_inicio_c(int c, string d, float p, int q, string es, string art, string alb){
	Cd *n = new Cd(c, d, p, q, es, art, alb);
	Cd *h = ch;
	em_comum_inserir(h, n); 	

}


void Lista::inserir_inicio_l(int c, string d, float p, int q, string titulo, string autor, string edit, int ano){
	Livro *n = new Livro(c, d, p, q, titulo, autor, edit, ano);
	Livro *h = lh;
	em_comum_inserir(h, n); 	

}


void Lista::inserir_inicio_s(int c, string d, float p, int q, float sod, bool g, bool lac, int val){
	Salgado *n = new Salgado(c, d, p, q, sod, g, lac, val);
	Salgado *h = sh;
	em_comum_inserir(h, n); 	

}


void Lista::inserir_inicio_d(int c, string d, float p, int q, float acu, bool g, bool lac, int val){
	Doce *n = new Doce(c, d, p, q, acu, g, lac, val);
	Doce *h = dh;
	em_comum_inserir(h, n); 	

}


void Lista::inserir_inicio_dv(int c, string d, float p, int q, string t, string ge, float dt){
	Dvd *n = new Dvd(c, d, p, q, t, ge, dt);
	Dvd *h = dvh;
	em_comum_inserir(h, n); 	

}

template<typename T>
int em_comum_verifica(T &n, T h){
	for(h = h->next; h; h = h->next){
		if((n == h) == true){
			cout << "n quant: " << h->get_quant() << endl;
			h->operator+(*n);
			cout << "n quant: " << h->get_quant();
			return 0;
		}
	}

	return 1;
}

void Lista::inserir_inicio_compra(int c, string d, float p, int q){
	Compra *n = new Compra(c, d, p, q);
	Compra *h = comph;

	if(em_comum_verifica(n, h) == 1){

		em_comum_inserir(h, n); 
	}	

}


void Lista::listar(){
	Bebida *b = bh->next;
	cout << "\n\n-----Setor de Bebida-----\n";
	for (; b != bt; b = b->next)
	{
		em_comum_listar(b);
	}

	Fruta *f = fh->next;
	cout << "\n\n-----Setor de Frutas-----\n";
	for (; f != ft; f = f->next)
	{
		em_comum_listar(f);
	}

	Salgado *s = sh->next;
	cout << "\n\n-----Setor de salgados-----\n";
	for(; s != st; s = s->next){

		em_comum_listar(s);	
	}


	Doce *d = dh->next;
	cout << "\n\n-----Setor de Doces-----\n";
	for(; d != dt; d = d->next){

		em_comum_listar(d);
	}

	Cd *c = ch->next;
	cout << "\n\n-----Setor de Cds-----\n";
	for(; c != ct; c = c->next){

		em_comum_listar(c);
		
	}

	Dvd *dv = dvh->next;
	cout << "\n\n-----Setor de Dvds-----\n";
	for(; dv != dvt; dv = dv->next){

		em_comum_listar(dv);		
	}

	Livro *l = lh->next;
	cout << "\n\n-----Setor de Livros-----\n";
	for(; l != lt; l = l->next){

		em_comum_listar(l);		
	}


}


int escolha_setor(){
	int c = 0;
	cout << "\n1 - Bebidas";
	cout << "\n2 - Frutas";
	cout << "\n3 - Salgados";
	cout << "\n4 - Doces"; 
	cout << "\n5 - Cds";
	cout << "\n6 - Dvds";
	cout << "\n7 - Livros\n\n";
	do{
		invalida(c);
	}while(c < 1 || c > 7);

	return c;
}


template<typename T>
int em_comum_consultar(T &search, string desc){
	int cont = 0;

	for (; search; search = search->next)
	{
		if (search->get_desc() == desc)
		{	
			em_comum_listar(search);
	
			cont++;
			goto fora;
		}
		
	}

	fora:
	if(cont==0) cout << "\nProduto não encontrado\n";

	return cont;

}


void Lista::consultar(string desc, int esc){
	int c = 0;
	cout << "\n\nEm qual setor de produtos deseja realizar a consulta: ";
	c = escolha_setor();

	if(esc == 1){
		cout << "\nInsira o nome do produto: ";
		cin.ignore();
	    getline(cin, desc, '\n');
	 }

	if(c == 1){
		Bebida *b = bh->next;
		if(esc == 2){
			while(b!=bt){
				em_comum_listar(b);
				cout << "Teor alcólico: " << b->get_teor() << "\nQuantidade de açúcar por ml: " << b->get_acucar();
		 		cout << "\nData de validade: " << b->get_validade() << endl;
		 		b = b->next;
			}

		}else if(em_comum_consultar(b, desc) > 0){
		 cout << "\nTeor alcólico: " << b->get_teor() << "\nQuantidade de açúcar por ml: " << b->get_acucar();
		 cout << "\nData de validade: " << b->get_validade() << endl;
		}

	}else if(c == 2){
		Fruta *f = fh->next;
		if(esc == 2){
			while(f!=ft){
				em_comum_listar(f);
				cout << "Número do lote: " << f->get_lote() << "\nData do lote: " << f->get_data();
				cout << "\nData de validade: " << f->get_validade() << endl;
				f = f->next;
			}

		}else if(em_comum_consultar(f, desc) > 0){
			cout << "\nNúmero do lote: " << f->get_lote() << "\nData do lote: " << f->get_data();
			cout << "\nData de validade: " << f->get_validade() << endl;
			}
		
	}else if(c == 3){
		Salgado *s = sh->next;
		if(esc == 2){
			while(s!=st){
				em_comum_listar(s);
				cout << "Quantidade de Sódio por miligrama: " << s->get_sodio() << "\nGlúten: ";
				if(s->get_gluten() == false){
					cout << "Não contém";
				}else{
					cout << "Contém";
				}

				cout << "\nLactose: ";
				 if(s->get_lactose() == false){
					cout << "Não contém";
				}else{
					cout << "Contém";
				}
				
				cout << "\nData de validade: " << s->get_validade() << endl;

				s = s->next;
			}

		}else if(em_comum_consultar(s, desc) > 0){
			cout << "\nQuantidade de Sódio por miligrama: " << s->get_sodio() << "\nGlúten: ";
			if(s->get_gluten() == false){
				cout << "Não contém";
			}else{
				cout << "Contém";
			}

			cout << "\nLactose: ";
			 if(s->get_lactose() == false){
				cout << "Não contém";
			}else{
				cout << "Contém";
			}

			cout << "\nData de validade: " << s->get_validade();			

		}

	}else if(c == 4){
		Doce *d = dh->next;
		if(esc == 2){
			while(d!=dt){
				em_comum_listar(d);
				cout << "Quantidade de Açúcar por miligrama: " << d->get_acucar() << "\nGlúten: ";

				if(d->get_gluten() == false){
					cout << "Não contém";
				}else{
					cout << "Contém";
				}

				cout << "\nLactose: ";
				 if(d->get_lactose() == false){
					cout << "Não contém";
				}else{
					cout << "Contém";
				}

				cout << "\nData de validade: " << d->get_validade() << endl;

				d = d->next;
			}

		}else if(em_comum_consultar(d, desc) > 0){
			cout << "\nQuantidade de Açúcar por miligrama: " << d->get_acucar() << "\nGlúten: ";

			if(d->get_gluten() == false){
				cout << "Não contém";
			}else{
				cout << "Contém";
			}

			cout << "\nLactose: ";
			 if(d->get_lactose() == false){
				cout << "Não contém";
			}else{
				cout << "Contém";
			}

			cout << "\nData de validade: " << d->get_validade();
		}

	}else if(c == 5){
		Cd *c = ch->next;
		if(esc == 2){
			while(c!=ct){
				em_comum_listar(c);
				cout << "Estilo: " << c->get_estilo() << "\nArtista: " << c->get_artista();
				cout << "\nAlbum: " << c->get_album() << endl;
				c = c->next;
			}
		}else if(em_comum_consultar(c, desc) > 0){
			cout << "\nEstilo: " << c->get_estilo() << "\nArtista: " << c->get_artista();
			cout << "\nAlbum: " << c->get_album() << endl;
		}

	}else if(c == 6){
		Dvd *dv = dvh->next;
		if(esc == 2){
			while(dv != dvt){
				em_comum_listar(dv);
				cout << "Título: " << dv->get_titulo() << "\nGênero: " << dv->get_genero();
				cout << "\nDuração total: " << dv->get_dt() << endl;		
				dv = dv->next;
			}
		}else if(em_comum_consultar(dv, desc) > 0){
			cout << "\nTítulo: " << dv->get_titulo() << "\nGênero: " << dv->get_genero();
			cout << "\nDuração total: " << dv->get_dt() << endl;		
		}

	}else if(c == 7){
		Livro *l = lh->next;
		if(esc == 2){
			while(l!=lt){
				em_comum_listar(l);
				cout << "Título: " << l->get_titulo() << "\nAutor: " << l->get_autor();
				cout << "\nEditora: " << l->get_editora() << "\nAno: " << l->get_ano() << endl;		
				l = l->next;
			}

		}else if(em_comum_consultar(l, desc) > 0){
		cout << "\nTítulo: " << l->get_titulo() << "\nAutor: " << l->get_autor();
		cout << "\nEditora: " << l->get_editora() << "\nAno: " << l->get_ano() << endl;		
		}
	}


}


template<typename T>
void em_comum_remover(T search, string desc){
	int cont = 0;
	cont = em_comum_consultar(search, desc);

	if(cont==0){
	 cout << "\nProduto não encontrado\n";

	}else{
		int escolha = 0;
		cout <<"\n\nO que deseja fazer: ";
		cout <<"\n1 - Remover todas as unidades";
		cout <<"\n2 - Remover uma quantidade específica\n\n";

		do{
			invalida(escolha);
		}while(escolha < 1 || escolha > 2);

		if(escolha == 1){
			search->prev->next = search->next;
			search->next->prev = search->prev;
			search->next = NULL;
			search->prev = NULL;
			cout << "\nTodos as unidades de " << search->get_desc() << " foram removidas\n";

		}else{
			cout << "\nInforme a quantidade de unidades de " << search->get_desc() << " a serem removidos: ";
			int q = 0;
			do{
				invalida(q);
				if (q >= search->get_quant())
					cout << "\n\nValor incorreto, por favor, digite um valor de 1 a " << search->get_quant()-1 << endl << endl;	

			}while(q >= search->get_quant());

			q = search->get_quant() - q; 
			
			search->set_quant(q);
			cout << "\nQuantidade removida\n";
		}

	}
}

void Lista::remover(string desc){
	int c = 0;
	cout << "\n\nEm qual setor de produtos deseja realizar a remoção: ";
	c = escolha_setor();

	cout << "\nInsira o nome do produto a ser removido: ";
	cin.ignore();
    getline(cin, desc, '\n');

	if(c == 1){
		Bebida *b = bh->next;
		em_comum_remover(b, desc);

	}else if(c == 2){
		Fruta *f = fh->next;
		em_comum_remover(f, desc);

	}else if(c == 3){
		Salgado *s = sh->next;
		em_comum_remover(s, desc);

	}else if(c == 4){
		Doce *d = dh->next;
		em_comum_remover(d, desc);

	}else if(c == 5){
		Cd *cd = ch->next;
		em_comum_remover(cd, desc);

	}else if(c == 6){
		Dvd *dv = dvh->next;
		em_comum_remover(dv, desc);

	}else if(c == 7){
		Livro *l = lh->next;
		em_comum_remover(l, desc);
	}
}

template<typename T>
void em_comum_alterar(T &altr){
	cout << "\n\n------Alteração------\n";
	cout << "\nCódigo de barras (6 dígitos): ";
	float codigo;
	invalida(codigo);
	altr->set_cdb(codigo);

	cout << "\nDescrição: ";
	string desc;
	cin.ignore();
    getline(cin, desc, '\n');
	altr->set_desc(desc);

	cout << "\nPreço: ";
	float preco;
	invalida(preco);
	altr->set_preco(preco);

	cout << "\nQuantidade: ";
	int qnt;
	invalida(qnt);
	altr->set_quant(qnt);

}

void Lista::alterar(string desc){
	int c = 0;
	int verifica = 0;
	cout << "\n\nEm qual setor de produtos deseja realizar a alteração: ";
	c = escolha_setor();

	cout << "\nInsira o nome do produto a ser alterado: ";
	cin.ignore();
    getline(cin, desc, '\n');
	if (c == 1){
		Bebida *b = bh->next;
		verifica = em_comum_consultar(b, desc);
		if(verifica > 0){
			em_comum_alterar(b);
			cout << "\nTeor alcólico: ";
			float teor;
			invalida(teor);
			b->set_teor(teor);

			cout << "\nQuantidade de açúcar por miligrama: ";
			float acucar;
			invalida(acucar);
			b->set_acucar(acucar);

			cout << "\nData de validade do produto: ";
			int validade;
			invalida(validade);
			b->set_validade(validade);
		}

	}else if(c == 2){
		Fruta *f = fh->next;
		verifica = em_comum_consultar(f, desc);

		if(verifica > 0){
			em_comum_alterar(f);
			cout << "\nNúmero do lote: ";
			int lote;
			invalida(lote);
			f->set_lote(lote);

			cout << "\nData do lote: ";
			int data;
			invalida(data);
			f->set_data(data);

			cout << "\nData de validade do produto: ";
			int validade;
			invalida(validade);
			f->set_validade(validade);
		}

	}else if(c == 3){
		Salgado *s = sh->next;
		verifica = em_comum_consultar(s, desc);

		if(verifica > 0){
			em_comum_alterar(s);

			cout << "\nQuantidade de sódio por miligrama: ";
			float so;
			invalida(so);
			s->set_sodio(so);

			cout << "\nContém glúten: ";
			int cg;
			alergicos(cg);
			s->set_gluten(cg);

			cout << "\nContém lactose: ";
			int cs;
			alergicos(cs);
			s->set_lactose(cs);

		}

	}else if(c == 4){
		Doce *d = dh->next;
		verifica = em_comum_consultar(d, desc);
		if(verifica > 0){
			em_comum_alterar(d);

			cout << "\nQuantidade de açúcar por miligrama: ";
			float so;
			invalida(so);
			d->set_acucar(so);

			cout << "\nContém glúten: ";
			int cg;
			alergicos(cg);
			d->set_gluten(cg);

			cout << "\nContém lactose: ";
			int cs;
			alergicos(cs);
			d->set_lactose(cs);

		}

	}else if(c == 5){
		Cd *cd = ch->next;
		verifica = em_comum_consultar(cd, desc);
		if(verifica > 0){
			em_comum_alterar(cd);
			
			cout << "\nEstilo: ";
			cin.ignore();
			string estilo;
	    	getline(cin, estilo, '\n');
			cd->set_estilo(estilo);

			cout << "\nArtista: ";
			string art;
	    	getline(cin, art, '\n');
			cd->set_artista(art);

			cout << "\nAlbum: ";
			string album;
	    	getline(cin, album, '\n');
			cd->set_album(album);
		} 

	}else if(c == 6){
		Dvd *dv = dvh->next;
		verifica = em_comum_consultar(dv, desc);
		if(verifica > 0){
			em_comum_alterar(dv);

			cout << "\nTítulo: ";
			string t;
			cin.ignore();
			getline(cin, t, '\n');
			dv->set_titulo(t);

			cout << "\nGênero: ";
			string ge;
			getline(cin, ge, '\n');
			dv->set_genero(ge);

			cout << "\nDuração total: ";
			float dt;
			invalida(dt);
			dv->set_dt(dt);

		}

	}else if(c == 7){
		Livro *l = lh->next;
		verifica = em_comum_consultar(l, desc);
		if(verifica > 0){
			em_comum_alterar(l);

			cout << "\nTítulo: ";
			string t;
			cin.ignore();
			getline(cin, t, '\n');
			l->set_titulo(t);

			cout << "\nAutor: ";
			string a;
			getline(cin, a, '\n');
			l->set_autor(a);

			cout << "\nEditora: ";
			string edit;
			getline(cin, edit, '\n');
			l->set_editora(edit);

			cout << "\nAno: ";
			int ano;
			invalida(ano);
			l->set_ano(ano);
		}
	}
}

template<typename T>
void em_comum_compra(T comp, string &d, int &cdb, float &preco, int &qnt){
	float valor = 0;
	cout << "\nComprar produto?";
	cout << "\n1 - sim";
	cout << "\n2 - não\n\n";
	int esc = 0;

	escolha_menu(esc, 1, 2);

	if(esc == 1){
		cout << "\nQuantas unidade deseja comprar? ";
		do{
			invalida(qnt);
			if(qnt > comp->get_quant()){
 				cout << "\n\nQuantidade indiponível em estoque, por favor, digite um valor de 1 a " << comp->get_quant() << endl << endl;	
			}

		}while(qnt > comp->get_quant());

		d = comp->get_desc();
		cdb = comp->get_cdb();
		preco = comp->get_preco();
		valor = qnt * preco;
		Compra::total += valor;
	}
}

void Lista::compra(){

	string d;
	int cdb;
	float preco;
	int qnt;

	int esc;
	cout << "\ninforme em qual setor deseja comprar\n";
	esc = escolha_setor();

	string desc;
	cout << "\nInforme o nome do produto que quer comprar: ";
	cin.ignore();
	getline(cin, desc, '\n');

	if(esc == 1){
		Bebida *b = bh->next;

		if(em_comum_consultar(b, desc) > 0){
		
			em_comum_compra(b, d, cdb, preco, qnt);

			inserir_inicio_compra(cdb, d, preco, qnt);
		}

	}else if(esc == 2){
		Fruta *f = fh->next;
		
		if(em_comum_consultar(f, desc) > 0){
		
			em_comum_compra(f, d, cdb, preco, qnt);

			inserir_inicio_compra(cdb, d, preco, qnt);
		}

	}else if(esc == 3){
		Salgado *s = sh->next;
		
		if(em_comum_consultar(s, desc) > 0){
		
			em_comum_compra(s, d, cdb, preco, qnt);

			inserir_inicio_compra(cdb, d, preco, qnt);
		}
 
	}else if(esc == 4){
		Doce *doc = dh->next;
		
		if(em_comum_consultar(doc, desc) > 0){
		
			em_comum_compra(doc, d, cdb, preco, qnt);

			inserir_inicio_compra(cdb, d, preco, qnt);
		}

	}else if(esc == 5){
		Cd *c = ch->next;
		
		if(em_comum_consultar(c, desc) > 0){
		
			em_comum_compra(c, d, cdb, preco, qnt);

			inserir_inicio_compra(cdb, d, preco, qnt);
		}

	}else if(esc == 6){
		Dvd *dv = dvh->next;
		
		if(em_comum_consultar(dv, desc) > 0){
		
			em_comum_compra(dv, d, cdb, preco, qnt);

			inserir_inicio_compra(cdb, d, preco, qnt);
		}

	}else if(esc == 7){
		Livro *l = lh->next;
		
		if(em_comum_consultar(l, desc) > 0){
		
			em_comum_compra(l, d, cdb, preco, qnt);

			inserir_inicio_compra(cdb, d, preco, qnt);
		}

	}

}

void Lista::lista_compras(){
	Compra *c = comph->next;

	for(; c != compt; c = c->next){
		em_comum_listar(c);	
	}
	cout << endl;
}