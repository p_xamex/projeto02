#include "produtos.h"

void Produto::set_cdb(int c){
	cdb = c;
}

int Produto::get_cdb(){
	return cdb;
}

void Produto::set_desc(string d){
	desc = d;
}

string Produto::get_desc(){
	return desc;
}

void Produto::set_preco(float p){
	preco = p;
}

float Produto::get_preco(){
	return preco;
}

void Produto::set_quant(int q){
	quant = q;
}

int Produto::get_quant(){
	return quant;
}