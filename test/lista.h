#ifndef LISTA_H
#define LISTA_H

#include <iostream>
using std::cout;
using std::cin;
using std::endl;

#include "bebidas.h"
#include "frutas.h"
#include "menu.h" 
#include "cds.h"
#include "livros.h"
#include "salgados.h"
#include "doces.h"
#include "dvds.h"
#include "compras.h"

class Lista {
	private:
		int quant;

	public:
		Bebida 	*bh;
		Bebida 	*bt;

		Fruta	*fh;
		Fruta	*ft;

		Cd 		*ch;
		Cd 		*ct;

		Livro	*lh;
		Livro	*lt;

		Salgado *sh;
		Salgado *st;

		Doce *dh;
		Doce *dt;

		Dvd 	*dvh;
		Dvd 	*dvt;

		Compra *comph;
		Compra *compt;

		Lista();
		~Lista();
		int get_quant();
		void inserir_inicio_b(int c, string d, float p, int q, float t, float a, int val);
		void inserir_inicio_f(int c, string d, float p, int q, int l, int da, int val);
		void inserir_inicio_s(int c, string d, float p, int q, float sod, bool g, bool lac, int val);
		void inserir_inicio_d(int c, string d, float p, int q, float acu, bool g, bool lac, int val);
		void inserir_inicio_c(int c, string d, float p, int q, string es, string art, string alb);
		void inserir_inicio_l(int c, string d, float p, int q, string titulo, string autor, string edit, int ano);
		void inserir_inicio_dv(int c, string d, float p, int q, string t, string ge, float dt);
		void inserir_inicio_compra(int c, string d, float p, int q);
		void listar();
		void consultar(string desc, int esc);
		void remover(string desc);
		void alterar(string desc);
		void compra();
		void lista_compras();
};



#endif