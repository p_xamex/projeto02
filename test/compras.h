#ifndef COMPRAS_H
#define COMPRAS_H

#include "produtos.h"
#include <string>
using std::string;

class Compra : public Produto{

	public:
		static float total;
		static int carrinho;
		Compra *next;
		Compra *prev;
		Compra();
		~Compra();

		Compra(int c, string d, float p, int q);
		void operator+ (Compra &c);
		bool operator== (Compra &c);
		void operator= (Compra &c);
	
};

#endif