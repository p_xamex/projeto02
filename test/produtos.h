#ifndef PRODUTOS_H
#define PRODUTOS_H

#include <string>
using std::string;

class Produto {
	protected:
		int cdb;
		string desc;
		float preco;
		int quant;
		
	public:
		static int lotes;
		void set_cdb(int c);
		int get_cdb();
		void set_desc(string d);
		string get_desc();
		void set_preco(float p);
		float get_preco();
		void set_quant(int q);
		int get_quant();

};


#endif