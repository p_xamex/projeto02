#include "compras.h"

Compra::Compra(){
	cdb = 0;
	desc = " ";
	preco = 0;
	quant = 0;
	next = NULL;
	prev = NULL;
	total = 0;
	carrinho = 0;
}

Compra::~Compra(){

}

Compra::Compra(int c, string d, float p, int q){
	set_cdb(c);
	set_desc(d);
	set_preco(p);
	set_quant(q);
	next = NULL;
	prev = NULL;
	carrinho += q;
}


void Compra::operator+ (Compra& c){
	quant += c.quant;
	preco += c.preco;
}

bool Compra::operator== (Compra& c){
	if(cdb == c.cdb){
		return true;
	}else{
		return false;
	}
}

void Compra::operator= (Compra& c){
	quant = c.quant;
	preco = c.preco;
	
}