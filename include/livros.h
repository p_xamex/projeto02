/**
* @file	 	livros.h
* @brief	Arquivo de cabeçalho contendo as definições dos métodos da classe Livro.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	01/06/2017
* @date		11/06/2017
*/



#ifndef LIVROS_H
#define LIVROS_H

#include <string>
using std::string;

#include "produtos.h"

class Livro : public Produto {
	private:
		string titulo;
		string autor;
		string editora;
		int ano;

	public:
		static int quant; // contador de quantidade de produtos da classe
		Livro *next; // aponta para o próximo nó
		Livro *prev; // aponta para o nó anterior

		/** 
		 * @brief	Construtor padrão da classe Livro
		 */
		Livro();


		/** 
		 * @brief	Destrutor padrão da classe Livro
		 */
		~Livro();


		/** 
		 * @brief	Construtor parametrizado da classe Livro
		 * @param	c código de barras do produto.
		 * @param	d descrição do produto.
		 * @param	p preço do produto.
		 * @param	q quantidade de unidades do produto.
		 * @param	titulo nome do título do livro.
		 * @param 	autor nome do autor do livro.
		 * @param	edit editora do livro.
		 * @param	ano ano de publicação do livro.
		 */
		Livro(int c, string d, float p, int q, string titulo, string autor, string editora, int ano);


		/** 
		 * @brief	Ḿétodo que altera o titulo do Livro
		 */
		void set_titulo(string titulo);


		/** 
		 * @brief	Ḿétodo que retorna o titulo do livro
		 * @return 	titulo do livro.
		 */
		string get_titulo();


		/** 
		 * @brief	Ḿétodo que altera o autor do Livro
		 */
		void set_autor(string autor);


		/** 
		 * @brief	Ḿétodo que retorna o autor do livro
		 * @return 	autor do livro.
		 */
		string get_autor();


		/** 
		 * @brief	Ḿétodo que altera a editora do Livro
		 */
		void set_editora(string editora);


		/** 
		 * @brief	Ḿétodo que retorna a editora do livro
		 * @return 	editora do livro.
		 */
		string get_editora();


		/** 
		 * @brief	Ḿétodo que altera o ano do Livro
		 */
		void set_ano(int ano);


		/** 
		 * @brief	Ḿétodo que retorna o ano do livro
		 * @return 	ano do livro.
		 */
		int get_ano();


		/** 
		 * @brief	Sobrecarga do operador == para para verificar se
		 *			existe código de barras na lista de produtos igual a 
		 *			do produto passado.
		 */
		bool operator== (Livro &c);
};

#endif