/**
* @file	 	bebidas.h
* @brief	Arquivo de cabeçalho contendo as definições dos métodos da classe Bebida.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	01/06/2017
* @date		11/06/2017
*/

#ifndef BEBIDAS_H
#define BEBIDAS_H

#include "produtos.h"
#include "perecivel.h"

class Bebida : public Produto, public Perecivel{
	private:
		float teor;
		float acucar;
	
	public:
		static int quant;
		Bebida *next;
		Bebida *prev;

		/** 
 		* @brief	Construtor padrão da classe Bebida
 		*/
		Bebida();


		/** 
 		* @brief	Construtor parametrizado da classe Bebida
 		* @param	c código de barras do produto.
		* @param	d descrição do produto.
		* @param	p preço do produto.
		* @param	q quantidade de unidades do produto.
		* @param	t teor alcólico da bebida.
		* @param	a quantidade de acucar por mg da bebida.
		* @param	val data de validade do produto.
 		*/
		Bebida(int c, string d, float p, int q, float t, float a, int val);

		/** 
 		* @brief	Destrutor padrão da classe Bebida
 		*/
		~Bebida();

		/**\defgroup Get_e_Sets_Bebida
		* @brief 	Métodos de get e set da classe Bebida
		* @{
		*/
		void set_teor(float t);
		float get_teor();
		void set_acucar(float a);
		float get_acucar();
		bool operator== (Bebida &c);
		/**
		* @}
		*/
};

#endif
