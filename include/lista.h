/**
* @file	 	lista.h
* @brief	Arquivo de cabeçalho contendo as definição da Classe lista,
*			responsável por gerenciar e alterar a lista de produtos e compras.
* @details 	A classe lista contém um par de ponteiros (head e tail) para cada 
*			tipo de produto e para a classe compra.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	01/06/2017
* @date		11/06/2017
*/

#ifndef LISTA_H
#define LISTA_H

#include <iostream>
using std::cout;
using std::cin;
using std::endl;

#include "util.h"
#include "em_comum.h"
#include "bebidas.h"
#include "frutas.h" 
#include "cds.h"
#include "livros.h"
#include "salgados.h"
#include "doces.h"
#include "dvds.h"
#include "compras.h"

class Lista {

	public:
		Bebida 	*bh;
		Bebida 	*bt;

		Fruta	*fh;
		Fruta	*ft;

		Cd 		*ch;
		Cd 		*ct;

		Livro	*lh;
		Livro	*lt;

		Salgado *sh;
		Salgado *st;

		Doce 	*dh;
		Doce 	*dt;

		Dvd 	*dvh;
		Dvd 	*dvt;

		Compra *comph;
		Compra *compt;


		/** 
		 * @brief	Construtor padrão da classe Lista
		 * @details instancia dinamicamente um par de objetos do tipo de cada
		 *			produto para servirem de sentinelas.
		 */
		Lista();


		/** 
		 * @brief	Destrutor padrão da classe Lista
		 * @details desaloca todos o nós das listas de
		 *			produtos e compras.
		 */
		~Lista();


		/** 
		 * @brief	Método responsável por adicionar um produto (bebida)
		 *			no início da lista de bebidas.
		 * @param	c código de barras do produto.
		 * @param	d descrição do produto.
		 * @param	p preço do produto.
		 * @param	q quantidade de unidades do produto.
		 * @param	t teor alcoólico da bebida
		 * @param	a quantidade de açúcar em miligramas.
		 * @param	val data validade do produto.
		 */
		void inserir_inicio_b(int c, string d, float p, int q, float t, float a, int val);


		/** 
		 * @brief	Método responsável por adicionar um produto (fruta)
		 *			no início da lista de frutas.
		 * @param	c código de barras do produto.
		 * @param	d descrição do produto.
		 * @param	p preço do produto.
		 * @param	q quantidade de unidades do produto.
		 * @param	l número do lote.
		 * @param	da data de produção do lote.
		 * @param	val data validade do produto.
		 */
		void inserir_inicio_f(int c, string d, float p, int q, int l, int da, int val);


		/** 
		 * @brief	Método responsável por adicionar um produto (salgado)
		 *			no início da lista de salgados.
		 * @param	c código de barras do produto.
		 * @param	d descrição do produto.
		 * @param	p preço do produto.
		 * @param	q quantidade de unidades do produto.
		 * @param	sod quantidade de sódio por miligrama.
		 * @param 	g indica se tem glútem ou não no protudo.
		 * @param	lac indica se tem lactose ou não no produto.
		 * @param	val data validade do produto.
		 */
		void inserir_inicio_s(int c, string d, float p, int q, float sod, bool g, bool lac, int val);


		/** 
		 * @brief	Método responsável por adicionar um produto (doce)
		 *			no início da lista de doces.
		 * @param	c código de barras do produto.
		 * @param	d descrição do produto.
		 * @param	p preço do produto.
		 * @param	q quantidade de unidades do produto.
		 * @param	acu quantidade de açúcar por miligrama.
		 * @param 	g indica se tem glútem ou não no protudo.
		 * @param	lac indica se tem lactose ou não no produto.
		 * @param	val data validade do produto.
		 */
		void inserir_inicio_d(int c, string d, float p, int q, float acu, bool g, bool lac, int val);


		/** 
		 * @brief	Método responsável por adicionar um produto (cd)
		 *			no início da lista de cds.
		 * @param	c código de barras do produto.
		 * @param	d descrição do produto.
		 * @param	p preço do produto.
		 * @param	q quantidade de unidades do produto.
		 * @param	es estilo do álbum.
		 * @param 	art nome do artista.
		 * @param	alb nome do álbum.
		 */
		void inserir_inicio_c(int c, string d, float p, int q, string es, string art, string alb);


		/** 
		 * @brief	Método responsável por adicionar um produto (livro)
		 *			no início da lista de livros.
		 * @param	c código de barras do produto.
		 * @param	d descrição do produto.
		 * @param	p preço do produto.
		 * @param	q quantidade de unidades do produto.
		 * @param	titulo nome do título do livro.
		 * @param 	autor nome do autor do livro.
		 * @param	edit editora do livro.
		 * @param	ano ano de publicação do livro.
		 */
		void inserir_inicio_l(int c, string d, float p, int q, string titulo, string autor, string edit, int ano);


		/** 
		 * @brief	Método responsável por adicionar um produto (dvd)
		 *			no início da lista de dvds.
		 * @param	c código de barras do produto.
		 * @param	d descrição do produto.
		 * @param	p preço do produto.
		 * @param	q quantidade de unidades do produto.
		 * @param	t nome do título do dvd.
		 * @param 	ge gênero do conteúdo do dvd.
		 * @param	dt duração total (em minutos) do dvd.
		 */
		void inserir_inicio_dv(int c, string d, float p, int q, string t, string ge, float dt);


		/** 
		 * @brief	Método responsável por adicionar um produto (compra)
		 *			no início da lista de compras.
		 * @details Antes de inserir uma compra na lista, checa se existe
		 			uma compra com mesmo código de barras no carrinho (operator==),
		 			se sim, soma a quantidade dos dois produtos (operator+).
		 * @param	c código de barras do produto.
		 * @param	d descrição do produto.
		 * @param	p preço do produto.
		 * @param	q quantidade de unidades do produto.
		 */
		void inserir_inicio_compra(int c, string d, float p, int q);


		/** 
		 * @brief	Método que lista todas os produtos cadastrados do estoque.
		 */
		void listar();


		/** 
		 * @brief	Método que consulta as informções de um determinado
		 *			produto ou setor de produtos.
		 * @param	esc escolha do usuário (setor ou produto).
		 */
		void consultar(int esc);


		/** 
		 * @brief	Método que remove um produto específico.
		 */
		void remover();

		/** 
		 * @brief	Método que altera um produto específico.
		 */
		void alterar();


		/** 
		 * @brief	Método que inicia a tarefa de compra, chamando as
		 *			funções e metódos que auxiliam a atividade.
		 */
		void compra();


		/** 
		 * @brief	Método que lista todas as compras feitas.
		 */
		void lista_compras();


		/** 
		 * @brief	Método responsável por checar se um produto com o mesmo
		 *			código de barras já foi registrado.
		 * @param	cdb código de barras do produto.
		 * @param	esc altera a função do método de acordo com o número pasado.
		 * @param	rem quantidade removida de um produto (necessária para a devolução 
		 *			de produtos.
		 */
		bool verifica_cdb(int cdb, int esc = 0, int rem = 0);


		/** 
		 * @brief	Método que remove uma compra específica.
		 */
		void remove_compra();
		
};



#endif