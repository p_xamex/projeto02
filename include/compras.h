/**
* @file	 	compras.h
* @brief	Arquivo de cabeçalho contendo as definições dos métodos da classe Compra.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	01/06/2017
* @date		11/06/2017
*/

#ifndef COMPRAS_H
#define COMPRAS_H

#include "produtos.h"
#include <string>
using std::string;

class Compra : public Produto{

	public:
		static float total; // contador do total gasto
		static int carrinho; // contador de número de compras
		Compra *next; // aponta para o próximo nó
		Compra *prev; // aponta para o nó anterior


		/** 
 		* @brief	Construtor padrão da classe Compra
 		*/
		Compra();


		/** 
 		* @brief	Destrutor padrão da classe Compra
 		*/
		~Compra();


		/** 
		 * @brief	Construtor parametrizado da classe Compra
		 * @param	c código de barras do produto.
		 * @param	d descrição do produto.
		 * @param	p preço do produto.
		 * @param	q quantidade de unidades do produto.
		 */
		Compra(int c, string d, float p, int q);


		/** 
		 * @brief	Sobrecarga do operador + para somar a quantidade 
		 *			de unidades de dois produtos.
		 * @details	soma a quantidade de um produto à do que chamou o método.
		 */
		void operator+ (Compra &c);


		/** 
		 * @brief	Sobrecarga do operador == para para verificar se
		 *			existe código de barras na lista de produtos igual a 
		 *			do produto passado.
		 */

		bool operator== (Compra &c);
	
};

#endif