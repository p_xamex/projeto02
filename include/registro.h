#ifndef REGISTRO_H
#define REGISTRO_H

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;

#include <fstream>
using std::ifstream;
using std::ofstream;

#include <string>

#include <limits>
using std::streamsize;

#include <ctime>

#include "lista.h"

class Registro {
	private:
		const string DIR = "./data/Estoque.csv";
		inline bool fileExists(const std::string& name);
		ifstream entrada;
		ofstream saida;
		void escreve(Bebida *bebida);
		void escreve(Doce *doce);
		void escreve(Salgado *salgado);
		void escreve(Fruta *fruta);
		void escreve(Livro *livro);
		void escreve(Cd *cd);
		void escreve(Dvd *dvd);
		template <typename produtoT>
		void escreverProduto(produtoT *produto, int tipo);
		void ler(Bebida *bebida, Lista &l);
		void ler(Doce *doce, Lista &l);
		void ler(Salgado *salgado, Lista &l);
		void ler(Fruta *fruta, Lista &l);
		void ler(Livro *livro, Lista &l);
		void ler(Cd *cd, Lista &l);
		void ler(Dvd *dvd, Lista &l);
		template <typename produtoT>
		void lerProduto(Lista &l, produtoT *produtoTipo, string produto, int tipo);
		void carregarRegistro(Lista &l);
		void criarRegistro(Lista &l);
		void fecharRegistro();
		void abrirRegistro();
		int dia;
		int mes;
		int ano;
	public:
		Registro(Lista &l);
		~Registro();
		void atualizarRegistro(Lista &l);

};

#endif