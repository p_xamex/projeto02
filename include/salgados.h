/**
* @file	 	salgados.h
* @brief	Arquivo de cabeçalho contendo as definições dos métodos da classe Salgado.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	01/06/2017
* @date		11/06/2017
*/
#ifndef SALGADOS_H
#define SALGADOS_H

#include <iostream>
using std::cout;

#include "produtos.h"
#include "perecivel.h"

class Salgado : public Produto, public Perecivel{
	private:
		float sodio;
		bool gluten;
		bool lactose;
		
	public:
		static int quant;
		Salgado *next;
		Salgado *prev;

		/** 
 		* @brief	Construtor padrão da classe Salgado
 		*/
		Salgado();

		/** 
 		* @brief	Destrutor padrão da classe Salgado
 		*/
		~Salgado();

		/** 
 		* @brief	Construtor parametrizado da classe Salgado
 		* @param	c código de barras do produto.
		* @param	d descrição do produto.
		* @param	p preço do produto.
		* @param	q quantidade de unidades do produto.
		* @param	sod quantidade de sodio no salgado.
		* @param	g booleana para determinar se contém gluten.
		* @param	lac booleana para determinar se contém lactose.
		* @param	val data de validade do produto.
 		*/
		Salgado(int c, string d, float p, int q, float sod, bool g, bool lac, int val);

		/**\defgroup Gets_e_Sets_Salgado
		* @brief 	Métodos de get e set da classe Salgado
		* @{
		*/
		void set_sodio(float s);
		float get_sodio();
		void set_gluten(int g);
		bool get_gluten();
		void set_lactose(int l);
		bool get_lactose();
		/**
		* @}
		*/


		/** 
		 * @brief	Sobrecarga do operador == para para verificar se
		 *			existe código de barras na lista de produtos igual a 
		 *			do produto passado.
		 */
		bool operator== (Salgado &c);
			
};

#endif