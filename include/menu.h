/**
* @file	 	menu.h
* @brief	Arquivo de cabeçalho contendo as definições das funções do menu,
*			responsável por gerenciar o menu de ações sobre a classe Lista.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	01/06/2017
* @date		11/06/2017
*/


#ifndef MENU_H
#define MENU_H

#include <iostream>
#include <string>
#include <limits>
#include "lista.h"
#include "util.h"


/** 
* @brief	Função resposável por receber informações do usuário e
*			chamar o metódo cadastrar correspondente passando essa informações.
* @param	l lista principal do programa.
*/
void cadastrar(Lista &l);


/** 
* @brief	Função resposável por tratar a escolha do usuário 
*			no menu principal.
* @param	l lista principal do programa.
* @param 	escolha ação escolhida no menu principal.
*/
void acessar_produto(Lista &l, int escolha);


/** 
* @brief	Função que controla as ações sobre a lista de compras
*			da classe Lista.
* @param	l lista principal do programa.
*/
void menu_compras(Lista &l);


/** 
* @brief	Função que controla o menu principal do programa e chama
*			as operações escolhidas pelo usuário.
* @param	l lista principal do programa.
*/
void acessar_lista_p(Lista &l);

#endif