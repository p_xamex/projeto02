/**
* @file	 	dvds.h
* @brief	Arquivo de cabeçalho contendo as definições dos métodos da classe Dvd.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	01/06/2017
* @date		11/06/2017
*/


#ifndef DVD_H
#define DVD_H

#include "produtos.h"
#include <string>
using std::string;

class Dvd : public Produto{
	private:
		string titulo;
		string genero;
		float d_total;
	public:
		static int quant; // contador de quantidade de produtos da classe
		Dvd *next; // aponta para o próximo nó
		Dvd *prev; // aponta para o nó anterior

		/** 
		 * @brief	Construtor padrão da classe Dvd
		 */
		Dvd();


		/** 
		 * @brief	Destrutor padrão da classe Dvd
		 */
		~Dvd();


		/** 
		 * @brief	Construtor parametrizado da classe Dvd
		 * @param	c código de barras do produto.
		 * @param	d descrição do produto.
		 * @param	p preço do produto.
		 * @param	q quantidade de unidades do produto.
		 * @param	t nome do título do dvd.
		 * @param 	ge gênero do conteúdo do dvd.
		 * @param	dt duração total (em minutos) do dvd.
		 */
		Dvd(int c, string d, float p, int q, string t, string ge, float dt);


		/** 
		 * @brief	Ḿétodo que altera o titulo do dvd
		 */
		void set_titulo(string t);


		/** 
		 * @brief	Ḿétodo que retorna o titulo do dvd
		 * @return 	titulo do dvd.
		 */
		string get_titulo();


		/** 
		 * @brief	Ḿétodo que altera o gênero do dvd
		 */
		void set_genero(string ge);


		/** 
		 * @brief	Ḿétodo que retorna o gênero do dvd
		 * @return 	gênero do dvd.
		 */
		string get_genero();


		/** 
		 * @brief	Ḿétodo que altera a duração total do dvd
		 */
		void set_dt(float dt);


		/** 
		 * @brief	Ḿétodo que retorna a duração total do dvd
		 * @return  a duração total do dvd.
		 */
		float get_dt();


		/** 
		 * @brief	Sobrecarga do operador == para para verificar se
		 *			existe código de barras na lista de produtos igual a 
		 *			do produto passado.
		 */
		bool operator== (Dvd &c);
	
};

#endif