/**
* @file     util.h
* @brief    Arquivo de cabẽçalho contendo as definições de funções que realizam
*           operações utilizadas em várias partes do programa.
* @author   Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author   Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since    01/06/2017
* @date     11/06/2017
*/


#ifndef UTIL_H
#define UTIL_H

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

#include <string>
#include <limits> //numeric_limits


/** 
* @brief    Função que limpa o terminal do linux usando ANSI escape codes
* @details  para mais detalher, acesse o link deixado como comentário ao lado do comando.
*/
void ClearScreen();


/** 
* @brief    Função que realiza o tratamento de todas as entradas do usuário
            verificando se são válidas ou não.
* @details  parte da função retirada do link: http://stackoverflow.com/questions/4798936/numeric-limits-was-not-declared-in-this-scope-no-matching-function-for-call-t
* @param    num int a ser testado
*/
void invalida(float &num);


/** 
* @brief    Função que realiza o tratamento de todas as entradas do usuário
            verificando se são válidas ou não.
* @details  parte da função retirada do link: http://stackoverflow.com/questions/4798936/numeric-limits-was-not-declared-in-this-scope-no-matching-function-for-call-t
* @param    num float a ser testado
*/
void invalida(int &num);


/** 
* @brief    Função que rertona se o produto contém ou não alérgico
            de acordo com a escolha passada.
* @param    a variável que armazena a escolha do usuário.
*/
bool alergicos(int &a);


/** 
* @brief    Função que faz o tratamento da escolha em menus.
* @param    esc recebe um valor digitado pelo usuário.
* @param    min valor mínimo do intervalo de opções do menu.
* @param    max valor máximo do intervalo de opções do menu.
*/
void escolha_menu(int &esc, int min, int max);


/** 
* @brief    Função que imprime a tabela de setores e requisita
            ao usuário uma escolha.
*/
int escolha_setor();

#endif