/**
* @file	 	em_comum.h
* @brief	Arquivo de cabeçalho contendo as definições das funções que realizam
*			operações em comum sobre todas as listas de produtos e compra.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	01/06/2017
* @date		11/06/2017
*/

#ifndef EM_COMUM_H
#define EM_COMUM_H

#include <string>
using std::string;


/** 
* @brief	Função template que recebe um tipo de produto e imprime suas
*			informações.
* @details  imprime apenas as informações que estão presentes em todos os tipos
*			de produtos.
* @param	h obejto que terá suas informações impressas.
*/
template<typename T>
void em_comum_listar(T h){
	if(h->get_quant() > 0) {
	    cout << "\nDescrição: " << h->get_desc() << "\nCódigo de barras: " << h->get_cdb();
	    cout << "\nPreço: " << h->get_preco() << "\nQuantidade: " << h->get_quant() << endl;
	}
}


/** 
* @brief	Função template que realiza a linkagem dos nós head e tail de uma lista.
*			informações.
* @param	h sentinela que aponta para o início da lista.
* @param	n sentinela que aponta para o final da lista.
*/
template<typename T>
void em_comum_declarar(T &h, T &t){
    h->next = t;
    t->prev = h;
    h->prev = NULL;
    t->next = NULL;
}


/** 
* @brief	Função template que realiza a linkagem dos nós head e tail de uma lista.
*			informações.
* @param	h sentinela que aponta para o início da lista.
* @param	n sentinela que aponta para o final da lista.
*/
template<typename T>
void em_comum_inserir(T &h, T &n){
    h->next->prev = n;
    n->next = h->next;
    h->next = n;
    n->prev = h;
}


/** 
* @brief	Função template que verifica se o código de barras do produto
*			passado já esta sendo utilizado por outro.
* @param	h sentinela que aponta para o início da lista.
* @param	n produto a ser verificado.
* @return 	retorna 1 caso não tenha encontrado e 0 caso tenha.
*/
template<typename T>
int verifica_compra(T &n, T h){
	for(h = h->next; h; h = h->next){
		if(*n == *h){
			*h + *n;
			return 0;
		}
	}

	return 1;
}


/** 
* @brief	Função template que consulta se um produto está na lista
*			do seu tipo respectivo de produto.
* @param	search produto a ser verificado.
* @return 	retorna cont, que indica se o produto foi encontrado ou não.
*			e também é utilizado para controle da mensagem: "produto não encontrado".
*/
template<typename T>
int em_comum_consultar(T &search){
	int cont;
	cont = 0;

	cout << "\nInforme o código de barras do produto: ";
	int cdb;
	invalida(cdb);


	for (; search; search = search->next)
	{
		if (search->get_cdb() == cdb)
		{	
			em_comum_listar(search);
	
			cont++;
			goto FORA; // goto utilizado para facilitar a para do laço
		}
		
	}

	FORA:
	if(cont==0) cout << "\nProduto não encontrado\n";

	return cont;

}


/** 
* @brief	Função template que remove uma quantidade específica ou todas
*			as unidades do protudo passado.
* @param	search produto a ser removido.
* @return 	número de unidades removidas.
*/
template<typename T>
int em_comum_remover(T search){
	int cont;
	cont = em_comum_consultar(search);

	int aux;
	aux = 0;

	if (cont > 0){

		int escolha;
		escolha = 0;

		cout <<"\n\nO que deseja fazer: ";
		cout <<"\n1 - Remover todas as unidades";
		cout <<"\n2 - Remover uma quantidade específica\n\n";

		do{
			invalida(escolha);
		}while(escolha < 1 || escolha > 2);

		if(escolha == 1){
			aux = search->get_quant();
			search->prev->next = search->next;
			search->next->prev = search->prev;
			search->next = NULL;
			search->prev = NULL;
			cout << "\nTodos as unidades de " << search->get_desc() << " foram removidas\n";

		}else{
			cout << "\nInforme a quantidade de unidades de " << search->get_desc() << " a serem removidos: ";
			int q = 0;
			do{
				invalida(q);
				if (q >= search->get_quant())
					cout << "\n\nValor incorreto, por favor, digite um valor de 1 a " << search->get_quant()-1 << endl << endl;	

			}while(q >= search->get_quant());

			aux = q;
			q = search->get_quant() - q; 
			
			search->set_quant(q);
			cout << "\nQuantidade removida\n";

			return aux;

		}

	}

	return aux;
}


/** 
* @brief	Função template que altera o produto passado.
* @param	altr produto a ser alterado.
*/
template<typename T>
void em_comum_alterar(T &altr){

	cout << "\nDescrição: ";
	string desc;
	cin.ignore();
    getline(cin, desc, '\n');
	altr->set_desc(desc);

	cout << "\nPreço: ";
	float preco;
	invalida(preco);
	altr->set_preco(preco);

	cout << "\nQuantidade: ";
	int qnt;
	invalida(qnt);
	altr->set_quant(qnt);

}


/** 
* @brief	Função template que realiza a compra do produto passado.
* @param	comp produto a ser comprado.
* @param	d recebe o nome do produto que será comprado
* @param	cdb recebe o código de barras do produto que será comprado
* @param	preco recebe o preço do produto que será comprado
* @param	qnt recebe a quantidade de unidades do produto que será comprado
* @param	total valor total da compra realizada (qnt. unidades x preço).
*/
template<typename T>
void em_comum_compra(T comp, string &d, int &cdb, float &preco, int &qnt, float &total){
	float valor = 0;
	cout << "\nComprar produto?";
	cout << "\n1 - sim";
	cout << "\n2 - não\n\n";
	int esc = 0;

	escolha_menu(esc, 1, 2);

	if(esc == 1){
		cout << "\nQuantas unidade deseja comprar? ";
		do{
			invalida(qnt);
			if(qnt > comp->get_quant()){
 				cout << "\n\nQuantidade indiponível em estoque, por favor, digite um valor de 1 a " << comp->get_quant() << endl << endl;	
			}

		}while(qnt > comp->get_quant());

		d = comp->get_desc();
		cdb = comp->get_cdb();
		preco = comp->get_preco();
		valor = qnt * preco;

		comp->set_quant(comp->get_quant() - qnt);

		total += valor;
	}
}


/** 
* @brief	Função template que verifica se existe produto com mesmo código
*			de barras já cadastrado, se sim, é somado a quantidade removida
*			desse produto a quantidade atual, simulando uma devolução.
* @param	cdb recebe o código de barras do produto que será comprado
* @param	h produto a ser alterado/consultado.
* @param 	cont usado para verificar se o produto foi achado ou não.
* @param	rem quantidade removida do protudo.
* @return 	número de unidades encontradas do produto.
*/
template<typename T>
int em_comum_verifica(int cdb, T &h, int &cont, int rem){
	T v = h->next;

	for(; v; v = v->next){
		if(cdb == v->get_cdb()){
			cont++;
			v->set_quant(v->get_quant() + rem);
		}
	}

	return cont;
}


/** 
* @brief	Função template que desaloca toda uma lista de produtos.
* @param	nh sentila para o início da lista passada.
*/
template<typename T>
void em_comum_desalocar(T &nh){
	while(nh){
		T n = nh->next;
		delete nh;
		nh = n;
	}
 
}

#endif