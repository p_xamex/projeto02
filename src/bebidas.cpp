/**
* @file	 	bebidas.cpp
* @brief	Arquivo de corpo contendo as implementações dos métodos da classe Bebida.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	01/06/2017
* @date		11/06/2017
*/
#include "bebidas.h"


/** 
* @brief	Construtor padrão da classe Bebida
*/
Bebida::Bebida(){
	teor = 0;
	acucar = 0;
	validade = 999999;
	cdb = 0;
	desc = " ";
	preco = 0;
	quant = 0;
	next = NULL;
	prev = NULL;
}

/** 
* @brief	Construtor parametrizado da classe Bebida
* @param	c código de barras do produto.
* @param	d descrição do produto.
* @param	p preço do produto.
* @param	q quantidade de unidades do produto.
* @param	t teor alcólico da bebida.
* @param	a quantidade de acucar por mg da bebida.
* @param	val data de validade do produto.
 		*/
Bebida::Bebida(int c, string d, float p, int q, float t, float a, int val){
	set_cdb(c);
	set_desc(d);
	set_preco(p);
	set_quant(q);
	set_teor(t);
	set_acucar(a); 
	set_validade(val);
	next = NULL;
	prev = NULL;
	this->lotes++;
	this->quant++;
	
}


/** 
* @brief	Destrutor padrão da classe Bebida
*/
Bebida::~Bebida(){

}

/**\defgroup Get_e_Sets_Bebida
* @brief 	Métodos de get e set da classe Bebida
* @{
*/
void Bebida::set_teor(float t){
	teor = t;  
}

float Bebida::get_teor(){
	return teor;
}

void Bebida::set_acucar(float a){
	acucar = a;
}

float Bebida::get_acucar(){
	return acucar;
}

/**
* @}
*/

bool Bebida::operator== (Bebida& c){
	if(cdb == c.cdb){
		return true;
	}else{
		return false;
	}
}