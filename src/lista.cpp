/**
* @file	 	lista.cpp
* @brief	Arquivo de corpo contendo as implementações da Classe lista,
*			responsável por gerenciar e alterar a lista de produtos e compras.
* @details 	A classe lista contém um par de ponteiros (head e tail) para cada 
*			tipo de produto e para a classe compra.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	01/06/2017
* @date		11/06/2017
*/


#include "lista.h"


/** 
 * @brief	Construtor padrão da classe Lista
 * @details instancia dinamicamente um par de objetos do tipo de cada
 *			produto para servirem de sentinelas.
 */
Lista::Lista() {
	bh = new Bebida(); 
	bt = new Bebida();
	em_comum_declarar(bh, bt);	
 
	fh = new Fruta();
	ft = new Fruta();
	em_comum_declarar(fh, ft);

	ch = new Cd();
	ct = new Cd();
	em_comum_declarar(ch, ct);

	lh = new Livro();
	lt = new Livro();
	em_comum_declarar(lh, lt);

	sh = new Salgado();
	st = new Salgado();
	em_comum_declarar(sh, st);

	dvh = new Dvd();
	dvt = new Dvd();
	em_comum_declarar(dvh, dvt);

	dh = new Doce();
	dt = new Doce();
	em_comum_declarar(dh, dt);

	comph = new Compra();
	compt = new Compra();
	
	comph->next = compt;
	compt->prev = comph;
	comph->prev = NULL;
	compt->next = NULL;

}


/** 
 * @brief	Destrutor padrão da classe Lista
 * @details desaloca todos o nós das listas de
 *			produtos e compras.
 */
Lista::~Lista(){
	em_comum_desalocar(bh);
	em_comum_desalocar(fh);
	em_comum_desalocar(sh);
	em_comum_desalocar(dh);
	em_comum_desalocar(ch);
	em_comum_desalocar(dvh);
	em_comum_desalocar(lh);
	em_comum_desalocar(comph);
	
}


/** 
 * @brief	Método responsável por adicionar um produto (bebida)
 *			no início da lista de bebidas.
 * @param	c código de barras do produto.
 * @param	d descrição do produto.
 * @param	p preço do produto.
 * @param	q quantidade de unidades do produto.
 * @param	t teor alcoólico da bebida
 * @param	a quantidade de açúcar em miligramas.
 * @param	val data validade do produto.
 */
void Lista::inserir_inicio_b(int c, string d, float p, int q, float t, float a, int val){
	Bebida *n = new Bebida(c, d, p, q, t, a, val);
	Bebida *h = bh;
	em_comum_inserir(h, n);


}


/** 
 * @brief	Método responsável por adicionar um produto (fruta)
 *			no início da lista de frutas.
 * @param	c código de barras do produto.
 * @param	d descrição do produto.
 * @param	p preço do produto.
 * @param	q quantidade de unidades do produto.
 * @param	l número do lote.
 * @param	da data de produção do lote.
 * @param	val data validade do produto.
 */
void Lista::inserir_inicio_f(int c, string d, float p, int q, int l, int da, int val){
	Fruta *n = new Fruta(c, d, p, q, l, da, val);
	Fruta *h = fh;
	em_comum_inserir(h, n);
}


/** 
 * @brief	Método responsável por adicionar um produto (salgado)
 *			no início da lista de salgados.
 * @param	c código de barras do produto.
 * @param	d descrição do produto.
 * @param	p preço do produto.
 * @param	q quantidade de unidades do produto.
 * @param	sod quantidade de sódio por miligrama.
 * @param 	g indica se tem glútem ou não no protudo.
 * @param	lac indica se tem lactose ou não no produto.
 * @param	val data validade do produto.
 */
void Lista::inserir_inicio_s(int c, string d, float p, int q, float sod, bool g, bool lac, int val){
	Salgado *n = new Salgado(c, d, p, q, sod, g, lac, val);
	Salgado *h = sh;
	em_comum_inserir(h, n); 	

}


/** 
 * @brief	Método responsável por adicionar um produto (doce)
 *			no início da lista de doces.
 * @param	c código de barras do produto.
 * @param	d descrição do produto.
 * @param	p preço do produto.
 * @param	q quantidade de unidades do produto.
 * @param	acu quantidade de açúcar por miligrama.
 * @param 	g indica se tem glútem ou não no protudo.
 * @param	lac indica se tem lactose ou não no produto.
 * @param	val data validade do produto.
 */
void Lista::inserir_inicio_d(int c, string d, float p, int q, float acu, bool g, bool lac, int val){
	Doce *n = new Doce(c, d, p, q, acu, g, lac, val);
	Doce *h = dh;
	em_comum_inserir(h, n); 	

}


/** 
 * @brief	Método responsável por adicionar um produto (cd)
 *			no início da lista de cds.
 * @param	c código de barras do produto.
 * @param	d descrição do produto.
 * @param	p preço do produto.
 * @param	q quantidade de unidades do produto.
 * @param	es estilo do álbum.
 * @param 	art nome do artista.
 * @param	alb nome do álbum.
 */
void Lista::inserir_inicio_c(int c, string d, float p, int q, string es, string art, string alb){
	Cd *n = new Cd(c, d, p, q, es, art, alb);
	Cd *h = ch;
	em_comum_inserir(h, n); 	

}


/** 
 * @brief	Método responsável por adicionar um produto (livro)
 *			no início da lista de livros.
 * @param	c código de barras do produto.
 * @param	d descrição do produto.
 * @param	p preço do produto.
 * @param	q quantidade de unidades do produto.
 * @param	titulo nome do título do livro.
 * @param 	autor nome do autor do livro.
 * @param	edit editora do livro.
 * @param	ano ano de publicação do livro.
 */
void Lista::inserir_inicio_l(int c, string d, float p, int q, string titulo, string autor, string edit, int ano){
	Livro *n = new Livro(c, d, p, q, titulo, autor, edit, ano);
	Livro *h = lh;
	em_comum_inserir(h, n); 	

}


/** 
 * @brief	Método responsável por adicionar um produto (dvd)
 *			no início da lista de dvds.
 * @param	c código de barras do produto.
 * @param	d descrição do produto.
 * @param	p preço do produto.
 * @param	q quantidade de unidades do produto.
 * @param	t nome do título do dvd.
 * @param 	ge gênero do conteúdo do dvd.
 * @param	dt duração total (em minutos) do dvd.
 */
void Lista::inserir_inicio_dv(int c, string d, float p, int q, string t, string ge, float dt){
	Dvd *n = new Dvd(c, d, p, q, t, ge, dt);
	Dvd *h = dvh;
	em_comum_inserir(h, n); 	

}


/** 
 * @brief	Método responsável por adicionar um produto (compra)
 *			no início da lista de compras.
 * @details Antes de inserir uma compra na lista, checa se existe
 			uma compra com mesmo código de barras no carrinho (operator==),
 			se sim, soma a quantidade dos dois produtos (operator+).
 * @param	c código de barras do produto.
 * @param	d descrição do produto.
 * @param	p preço do produto.
 * @param	q quantidade de unidades do produto.
 */
void Lista::inserir_inicio_compra(int c, string d, float p, int q){
	Compra *n = new Compra(c, d, p, q);
	Compra *h = comph;

	if(verifica_compra(n, h) == 1){

		em_comum_inserir(h, n); 
	}	

}


/** 
 * @brief	Método que lista todas os produtos cadastrados do estoque.
 */
void Lista::listar(){
	Bebida *b = bh->next;
	cout << "\n\n-----Setor de Bebida-----\n";
	for (; b != bt; b = b->next)
	{
		em_comum_listar(b);
	}

	Fruta *f = fh->next;
	cout << "\n\n-----Setor de Frutas-----\n";
	for (; f != ft; f = f->next)
	{
		em_comum_listar(f);
	}

	Salgado *s = sh->next;
	cout << "\n\n-----Setor de salgados-----\n";
	for(; s != st; s = s->next){

		em_comum_listar(s);	
	}


	Doce *d = dh->next;
	cout << "\n\n-----Setor de Doces-----\n";
	for(; d != dt; d = d->next){

		em_comum_listar(d);
	}

	Cd *c = ch->next;
	cout << "\n\n-----Setor de Cds-----\n";
	for(; c != ct; c = c->next){

		em_comum_listar(c);
		
	}

	Dvd *dv = dvh->next;
	cout << "\n\n-----Setor de Dvds-----\n";
	for(; dv != dvt; dv = dv->next){

		em_comum_listar(dv);		
	}

	Livro *l = lh->next;
	cout << "\n\n-----Setor de Livros-----\n";
	for(; l != lt; l = l->next){

		em_comum_listar(l);		
	}


}


/** 
 * @brief	Método que consulta as informções de um determinado
 *			produto ou setor de produtos.
 * @param	esc escolha do usuário (setor ou produto).
 */
void Lista::consultar(int esc){
	int c = 0;
	cout << "\n\nEm qual setor de produtos deseja realizar a consulta: ";
	c = escolha_setor();

	if(c == 1){
		Bebida *b = bh->next;
		if(esc == 2){
			while(b!=bt){
				em_comum_listar(b);
				cout << "Teor alcólico: " << b->get_teor() << "\nQuantidade de açúcar por ml: " << b->get_acucar();
		 		cout << "\nData de validade: " << b->get_validade() << endl;
		 		b = b->next;
			}

		}else if(em_comum_consultar(b) > 0){
		 cout << "Teor alcólico: " << b->get_teor() << "\nQuantidade de açúcar por ml: " << b->get_acucar();
		 cout << "\nData de validade: " << b->get_validade() << endl;
		}

	}else if(c == 2){
		Fruta *f = fh->next;
		if(esc == 2){
			while(f!=ft){
				em_comum_listar(f);
				cout << "Número do lote: " << f->get_lote() << "\nData do lote: " << f->get_data();
				cout << "\nData de validade: " << f->get_validade() << endl;
				f = f->next;
			}

		}else if(em_comum_consultar(f) > 0){
			cout << "Número do lote: " << f->get_lote() << "\nData do lote: " << f->get_data();
			cout << "\nData de validade: " << f->get_validade() << endl;
			}
		
	}else if(c == 3){
		Salgado *s = sh->next;
		if(esc == 2){
			while(s!=st){
				em_comum_listar(s);
				cout << "Quantidade de Sódio por miligrama: " << s->get_sodio() << "\nGlúten: ";
				if(s->get_gluten() == false){
					cout << "Não contém";
				}else{
					cout << "Contém";
				}

				cout << "\nLactose: ";
				 if(s->get_lactose() == false){
					cout << "Não contém";
				}else{
					cout << "Contém";
				}
				
				cout << "\nData de validade: " << s->get_validade() << endl;

				s = s->next;
			}

		}else if(em_comum_consultar(s) > 0){
			cout << "Quantidade de Sódio por miligrama: " << s->get_sodio() << "\nGlúten: ";
			if(s->get_gluten() == false){
				cout << "Não contém";
			}else{
				cout << "Contém";
			}

			cout << "\nLactose: ";
			 if(s->get_lactose() == false){
				cout << "Não contém";
			}else{
				cout << "Contém";
			}

			cout << "\nData de validade: " << s->get_validade();			

		}

	}else if(c == 4){
		Doce *d = dh->next;
		if(esc == 2){
			while(d!=dt){
				em_comum_listar(d);
				cout << "Quantidade de Açúcar por miligrama: " << d->get_acucar() << "\nGlúten: ";

				if(d->get_gluten() == false){
					cout << "Não contém";
				}else{
					cout << "Contém";
				}

				cout << "\nLactose: ";
				 if(d->get_lactose() == false){
					cout << "Não contém";
				}else{
					cout << "Contém";
				}

				cout << "\nData de validade: " << d->get_validade() << endl;

				d = d->next;
			}

		}else if(em_comum_consultar(d) > 0){
			cout << "Quantidade de Açúcar por miligrama: " << d->get_acucar() << "\nGlúten: ";

			if(d->get_gluten() == false){
				cout << "Não contém";
			}else{
				cout << "Contém";
			}

			cout << "\nLactose: ";
			 if(d->get_lactose() == false){
				cout << "Não contém";
			}else{
				cout << "Contém";
			}

			cout << "\nData de validade: " << d->get_validade();
		}

	}else if(c == 5){
		Cd *c = ch->next;
		if(esc == 2){
			while(c!=ct){
				em_comum_listar(c);
				cout << "Estilo: " << c->get_estilo() << "\nArtista: " << c->get_artista();
				cout << "\nAlbum: " << c->get_album() << endl;
				c = c->next;
			}
		}else if(em_comum_consultar(c) > 0){
			cout << "Estilo: " << c->get_estilo() << "\nArtista: " << c->get_artista();
			cout << "\nAlbum: " << c->get_album() << endl;
		}

	}else if(c == 6){
		Dvd *dv = dvh->next;
		if(esc == 2){
			while(dv != dvt){
				em_comum_listar(dv);
				cout << "Título: " << dv->get_titulo() << "\nGênero: " << dv->get_genero();
				cout << "\nDuração total: " << dv->get_dt() << endl;		
				dv = dv->next;
			}
		}else if(em_comum_consultar(dv) > 0){
			cout << "\nTítulo: " << dv->get_titulo() << "\nGênero: " << dv->get_genero();
			cout << "\nDuração total: " << dv->get_dt() << endl;		
		}

	}else if(c == 7){
		Livro *l = lh->next;
		if(esc == 2){
			while(l!=lt){
				em_comum_listar(l);
				cout << "Título: " << l->get_titulo() << "\nAutor: " << l->get_autor();
				cout << "\nEditora: " << l->get_editora() << "\nAno: " << l->get_ano() << endl;		
				l = l->next;
			}

		}else if(em_comum_consultar(l) > 0){
		cout << "Título: " << l->get_titulo() << "\nAutor: " << l->get_autor();
		cout << "\nEditora: " << l->get_editora() << "\nAno: " << l->get_ano() << endl;		
		}
	}


}


/** 
 * @brief	Método que remove um produto específico.
 */
void Lista::remover(){
	int c = 0;
	cout << "\n\nEm qual setor de produtos deseja realizar a remoção: ";
	c = escolha_setor();

	if(c == 1){
		Bebida *b = bh->next;
		em_comum_remover(b);

	}else if(c == 2){
		Fruta *f = fh->next;
		em_comum_remover(f);

	}else if(c == 3){
		Salgado *s = sh->next;
		em_comum_remover(s);

	}else if(c == 4){
		Doce *d = dh->next;
		em_comum_remover(d);

	}else if(c == 5){
		Cd *cd = ch->next;
		em_comum_remover(cd);

	}else if(c == 6){
		Dvd *dv = dvh->next;
		em_comum_remover(dv);

	}else if(c == 7){
		Livro *l = lh->next;
		em_comum_remover(l);
	}

	Produto::lotes--;
}


/** 
 * @brief	Método que altera um produto específico.
 */
void Lista::alterar(){
	float codigo;
	codigo = 0;

	int c;
	c = 0;

	int verifica;
	verifica = 0;

	cout << "\n\nEm qual setor de produtos deseja realizar a alteração: ";
	c = escolha_setor();

	cout << "\n\n------Alteração------\n";
	if (c == 1){
		Bebida *b = bh->next;
		verifica = em_comum_consultar(b);

		if(verifica > 0){
			cout << "\nCódigo de barras (6 dígitos): ";
			invalida(codigo);

			if (verifica_cdb(codigo) == false){
				b->set_cdb(codigo);
				em_comum_alterar(b);
				cout << "\nTeor alcólico: ";
				float teor;
				invalida(teor);
				b->set_teor(teor);

				cout << "\nQuantidade de açúcar por miligrama: ";
				float acucar;
				invalida(acucar);
				b->set_acucar(acucar);

				cout << "\nData de validade do produto: ";
				int validade;
				invalida(validade);
				b->set_validade(validade);
			}
		}

	}else if(c == 2){
		Fruta *f = fh->next;
		verifica = em_comum_consultar(f);

		if(verifica > 0){
			cout << "\nCódigo de barras (6 dígitos): ";
			invalida(codigo);

			if (verifica_cdb(codigo) == false){
				f->set_cdb(codigo);
				em_comum_alterar(f);
				cout << "\nNúmero do lote: ";
				int lote;
				invalida(lote);
				f->set_lote(lote);

				cout << "\nData do lote: ";
				int data;
				invalida(data);
				f->set_data(data);

				cout << "\nData de validade do produto: ";
				int validade;
				invalida(validade);
				f->set_validade(validade);
			}
		}

	}else if(c == 3){
		Salgado *s = sh->next;
		verifica = em_comum_consultar(s);

		if(verifica > 0){
			cout << "\nCódigo de barras (6 dígitos): ";
			invalida(codigo);

			if (verifica_cdb(codigo) == false){
				s->set_cdb(codigo);
				em_comum_alterar(s);

				cout << "\nQuantidade de sódio por miligrama: ";
				float so;
				invalida(so);
				s->set_sodio(so);

				cout << "\nContém glúten: ";
				int cg;
				alergicos(cg);
				s->set_gluten(cg);

				cout << "\nContém lactose: ";
				int cs;
				alergicos(cs);
				s->set_lactose(cs);
			}

		}

	}else if(c == 4){
		Doce *d = dh->next;
		verifica = em_comum_consultar(d);

		if(verifica > 0){
			cout << "\nCódigo de barras (6 dígitos): ";
			invalida(codigo);

			if (verifica_cdb(codigo) == false){
				d->set_cdb(codigo);
				em_comum_alterar(d);

				cout << "\nQuantidade de açúcar por miligrama: ";
				float so;
				invalida(so);
				d->set_acucar(so);

				cout << "\nContém glúten: ";
				int cg;
				alergicos(cg);
				d->set_gluten(cg);

				cout << "\nContém lactose: ";
				int cs;
				alergicos(cs);
				d->set_lactose(cs);
			}

		}

	}else if(c == 5){
		Cd *cd = ch->next;
		verifica = em_comum_consultar(cd);

		if(verifica > 0){
			cout << "\nCódigo de barras (6 dígitos): ";
			invalida(codigo);

			if (verifica_cdb(codigo) == false){
				cd->set_cdb(codigo);
				em_comum_alterar(cd);
				
				cout << "\nEstilo: ";
				cin.ignore();
				string estilo;
		    	getline(cin, estilo, '\n');
				cd->set_estilo(estilo);

				cout << "\nArtista: ";
				string art;
		    	getline(cin, art, '\n');
				cd->set_artista(art);

				cout << "\nAlbum: ";
				string album;
		    	getline(cin, album, '\n');
				cd->set_album(album);
			}
		} 

	}else if(c == 6){
		Dvd *dv = dvh->next;
		verifica = em_comum_consultar(dv);

		if(verifica > 0){
			cout << "\nCódigo de barras (6 dígitos): ";
			invalida(codigo);

			if (verifica_cdb(codigo) == false){
				dv->set_cdb(codigo);
				em_comum_alterar(dv);

				cout << "\nTítulo: ";
				string t;
				cin.ignore();
				getline(cin, t, '\n');
				dv->set_titulo(t);

				cout << "\nGênero: ";
				string ge;
				getline(cin, ge, '\n');
				dv->set_genero(ge);

				cout << "\nDuração total: ";
				float dt;
				invalida(dt);
				dv->set_dt(dt);
			}

		}

	}else if(c == 7){
		Livro *l = lh->next;
		verifica = em_comum_consultar(l);

		if(verifica > 0){
			cout << "\nCódigo de barras (6 dígitos): ";
			invalida(codigo);

			if (verifica_cdb(codigo) == false){
				l->set_cdb(codigo);
				em_comum_alterar(l);

				cout << "\nTítulo: ";
				string t;
				cin.ignore();
				getline(cin, t, '\n');
				l->set_titulo(t);

				cout << "\nAutor: ";
				string a;
				getline(cin, a, '\n');
				l->set_autor(a);

				cout << "\nEditora: ";
				string edit;
				getline(cin, edit, '\n');
				l->set_editora(edit);

				cout << "\nAno: ";
				int ano;
				invalida(ano);
				l->set_ano(ano);
			}
		}
	}
}


/** 
 * @brief	Método que inicia a tarefa de compra, chamando as
 *			funções e metódos que auxiliam a atividade.
 */
void Lista::compra(){

	string d;
	int cdb;
	float preco;
	int qnt;

	int esc;
	cout << "\ninforme em qual setor deseja comprar\n";
	esc = escolha_setor();

	if(esc == 1){
		Bebida *b = bh->next;

		if(em_comum_consultar(b) > 0){
		
			em_comum_compra(b, d, cdb, preco, qnt, Compra::total);

			inserir_inicio_compra(cdb, d, preco, qnt);
		}

	}else if(esc == 2){
		Fruta *f = fh->next;
		
		if(em_comum_consultar(f) > 0){
		
			em_comum_compra(f, d, cdb, preco, qnt, Compra::total);

			inserir_inicio_compra(cdb, d, preco, qnt);
		}

	}else if(esc == 3){
		Salgado *s = sh->next;
		
		if(em_comum_consultar(s) > 0){
		
			em_comum_compra(s, d, cdb, preco, qnt, Compra::total);

			inserir_inicio_compra(cdb, d, preco, qnt);
		}
 
	}else if(esc == 4){
		Doce *doc = dh->next;
		
		if(em_comum_consultar(doc) > 0){
		
			em_comum_compra(doc, d, cdb, preco, qnt, Compra::total);

			inserir_inicio_compra(cdb, d, preco, qnt);
		}

	}else if(esc == 5){
		Cd *c = ch->next;
		
		if(em_comum_consultar(c) > 0){
		
			em_comum_compra(c, d, cdb, preco, qnt, Compra::total);

			inserir_inicio_compra(cdb, d, preco, qnt);
		}

	}else if(esc == 6){
		Dvd *dv = dvh->next;
		
		if(em_comum_consultar(dv) > 0){
		
			em_comum_compra(dv, d, cdb, preco, qnt, Compra::total);

			inserir_inicio_compra(cdb, d, preco, qnt);
		}

	}else if(esc == 7){
		Livro *l = lh->next;
		
		if(em_comum_consultar(l) > 0){
		
			em_comum_compra(l, d, cdb, preco, qnt, Compra::total);

			inserir_inicio_compra(cdb, d, preco, qnt);
		}

	}

}


/** 
 * @brief	Método que lista todas as compras feitas.
 */
void Lista::lista_compras(){
	Compra *c = comph->next;

	for(; c != compt; c = c->next){
		cout << c->get_desc() << endl;
		cout << ' ' << c->get_quant() << " UN X "<< c->get_preco();
		cout << "\t\t\t" << c->get_quant() * c->get_preco() << endl << endl;  		
			
	}
	cout << endl;
}


/** 
 * @brief	Método responsável por checar se um produto com o mesmo
 *			código de barras já foi registrado.
 * @param	cdb código de barras do produto.
 * @param	esc altera a função do método de acordo com o número pasado.
 * @param	rem quantidade removida de um produto (necessária para a devolução 
 *			de produtos.
 */
bool Lista::verifica_cdb(int cdb, int esc, int rem){
	int cont;
	cont = 0;

	em_comum_verifica(cdb, bh, cont, rem);

	em_comum_verifica(cdb, fh, cont, rem);

	em_comum_verifica(cdb, sh, cont, rem);

	em_comum_verifica(cdb, dh, cont, rem);

	em_comum_verifica(cdb, ch, cont, rem);

	em_comum_verifica(cdb, dvh, cont, rem);

	em_comum_verifica(cdb, lh, cont, rem);

	if(cont > 0 && esc == 0){
		cout << "\nAtenção! Produto com mesmo código de barras já cadastrado!\ntente novamente\n";
		return true;
	}

	return false;
}


/** 
 * @brief	Método que remove uma compra específica.
 */
void Lista::remove_compra(){
	Compra *comp = comph->next;

	int rem;
	rem = em_comum_remover(comp);

	int cdb;
	cdb = comp->get_cdb();

	Compra::total -= rem * comp->get_preco();
	Compra::carrinho -= rem;

	verifica_cdb(cdb, 1, rem);

}