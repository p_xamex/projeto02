/**
* @file	 	produtos.cpp
* @brief	Arquivo de corpo contendo as implementações dos métodos da classe Produtos.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	01/06/2017
* @date		11/06/2017
*/

#include "produtos.h"



/** 
 * @brief	Ḿétodo que altera o valor do código de barras
 */
void Produto::set_cdb(int c){
	cdb = c;
}


/** 
 * @brief	Ḿétodo que retorna o código de barras do produto.
 * @return 	código de barras do produto.
 */
int Produto::get_cdb(){
	return cdb;
}


/** 
 * @brief	Ḿétodo que altera a descrição do protudo
 */
void Produto::set_desc(string d){
	desc = d;
}


/** 
 * @brief	Ḿétodo que retorna a descrição do produto.
 * @return 	descrição do produto.
 */
string Produto::get_desc(){
	return desc;
}


/** 
 * @brief	Ḿétodo que altera o valor do preço.
 */
void Produto::set_preco(float p){
	preco = p;
}


/** 
 * @brief	Ḿétodo que retorna o preço do produto.
 * @return 	preço do produto.
 */
float Produto::get_preco(){
	return preco;
}


/** 
 * @brief	Ḿétodo que altera a quantidade de unidades do protudo.
 */
void Produto::set_quant(int q){
	quant = q;
}


/** 
 * @brief	Ḿétodo que retorna a quantidade de unidades do protudo.
 * @return 	quantidade de unidades do protudo.
 */
int Produto::get_quant(){
	return quant;
}