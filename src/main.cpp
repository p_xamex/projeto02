/**
* @file	 	main.cpp
* @brief	Programa que realiza cadastro e venda de produtos para a loja
*			de conveniência QLeveTudo.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	01/06/2017
* @date		11/06/2017
*/

#include <iostream>
#include "menu.h" 
#include "util.h"
#include "lista.h"
#include "produtos.h"
#include "registro.h"

//Inicialização das variáveis estáticas usadas para a contagem
// da quantide total de objetos das respectivas classes.
float Compra::total = 0;
int Produto::lotes = 0;
int Bebida::quant = 0;
int Cd::quant = 0;
int Doce::quant = 0;
int Dvd::quant = 0;
int Fruta::quant = 0;
int Livro::quant = 0;
int Salgado::quant = 0;

int main() {
	Lista l;
	Registro reg(l);
	acessar_lista_p(l);
	reg.atualizarRegistro(l);

	return 0;
}
