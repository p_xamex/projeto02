/**
* @file	 	menu.cpp
* @brief	Arquivo de corpo contendo as implementações das funções do menu,
*			responsável por gerenciar o menu de ações sobre a classe Lista.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author 	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	01/06/2017
* @date		11/06/2017
*/


#include "menu.h"

int Compra::carrinho = 0;


/** 
* @brief	Função resposável por receber informações do usuário e
*			chamar o metódo cadastrar correspondente passando essa informações.
* @param	l lista principal do programa.
*/
void cadastrar(Lista &l){
	ClearScreen();

	cout << "Qual tipo de produto deseja cadastrar:\n";
	cout << "\n1 - Bebida";
	cout << "\n2 - Fruta";
	cout << "\n3 - Salgado";
	cout << "\n4 - Doce";
	cout << "\n5 - Cd";
	cout << "\n6 - Dvd";
	cout << "\n7 - Livro\n\n";


	int escolha;
	escolha_menu(escolha, 1, 7);

	float codigo = 0;
	do{
		cout << "\nCódigo de barras (6 dígitos): ";
		invalida(codigo);
		
	}while(l.verifica_cdb(codigo, 0, 0) > 0);
	cout << "\nDescrição: ";
	string desc;
	cin.ignore();
    getline(cin, desc, '\n');

	cout << "\nPreço: ";
	float preco;
	invalida(preco);

	cout << "\nQuantidade: ";
	int qnt;
	invalida(qnt);

	if(escolha == 1){
		cout << "\nTeor alcólico: ";
		float teor;
		invalida(teor);

		cout << "\nQuantidade de açúcar por miligrama: ";
		float acucar;
		invalida(acucar);

		cout << "\nData de validade do produto: ";
		int validade;
		invalida(validade);

		l.inserir_inicio_b(codigo, desc, preco, qnt, teor, acucar, validade);
	
	}else if(escolha == 2){
		cout << "\nNúmero do lote: ";
		int lote;
		invalida(lote);

		cout << "\nData do lote: ";
		int data;
		invalida(data);

		cout << "\nData de validade do produto: ";
		int validade;
		invalida(validade);

		l.inserir_inicio_f(codigo, desc, preco, qnt, lote, data, validade);
	
	}else if(escolha == 3){
		cout << "\nQuantidade de Sódio por miligrama: ";
		float so;
		invalida(so);

		cout << "\nContém glúten: ";
		int cg;
		bool g;
		g = alergicos(cg);

		cout << "\nContém lactose: ";
		int cs;
		bool s;
		s = alergicos(cs);

		cout << "\nData de validade do produto: ";
		int validade;
		invalida(validade);

		l.inserir_inicio_s(codigo, desc, preco, qnt, so, g, s, validade);

	}else if(escolha == 4){

		cout << "\nQuantidade de açúcar por miligrama: ";
		float acu;
		invalida(acu);

		cout << "\nContém glúten: ";
		int cg;
		bool g;
		g = alergicos(cg);

		cout << "\nContém lactose: ";
		int cl;
		bool lac;
		lac = alergicos(cl);


		cout << "\nData de validade do produto: ";
		int val;
		invalida(val);

		l.inserir_inicio_d(codigo, desc, preco, qnt, acu, g, lac, val);


	}else if(escolha == 5){
		cout << "\nEstilo: ";
		string estilo;
		cin.ignore();
    	getline(cin, estilo, '\n');

		cout << "\nArtista: ";
		string art;
    	getline(cin, art, '\n');

		cout << "\nAlbum: ";
		string album;
    	getline(cin, album, '\n');

		l.inserir_inicio_c(codigo, desc, preco, qnt, estilo, art, album);

	}else if(escolha == 6){
		cout << "\nTítulo: ";
		string t;
		cin.ignore();
		getline(cin, t, '\n');

		cout << "\nGênero: ";
		string ge;
		getline(cin, ge, '\n');

		cout << "\nDuração total: ";
		float dt;
		invalida(dt);

		l.inserir_inicio_dv(codigo, desc, preco, qnt, t, ge, dt);

	}else if(escolha == 7){

			cout << "\nTítulo: ";
			string t;
			cin.ignore();
			getline(cin, t, '\n');
			
			cout << "\nAutor: ";
			string a;
			getline(cin, a, '\n');

			cout << "\nEditora: ";
			string edit;
			getline(cin, edit, '\n');
			
			cout << "\nAno: ";
			int ano;
			invalida(ano);
			
			l.inserir_inicio_l(codigo, desc, preco, qnt, t, a, edit, ano);
	}

	ClearScreen();
}


/** 
* @brief	Função resposável por tratar a escolha do usuário 
*			no menu principal.
* @param	l lista principal do programa.
* @param 	escolha ação escolhida no menu principal.
*/
void acessar_produto(Lista &l, int escolha){
	int c;
	if(escolha == 3){
		cout << "\nConsultar um produto específico ou um setor de produtos?";
		cout << "\n1 - Produto";
		cout << "\n2 - Setor\n\n";
		escolha_menu(c, 1, 2);

		l.consultar(c);
	}else if(escolha == 4){
		l.remover();
	}else if(escolha == 5){
		l.alterar();
	}
}


/** 
* @brief	Função que controla as ações sobre a lista de compras
*			da classe Lista.
* @param	l lista principal do programa.
*/
void menu_compras(Lista &l){
	ClearScreen();
	int pare;
	pare = 1;
	while(pare == 1){
		cout <<"\n----------Menu de Compras----------\n";
		cout <<"\n1 - Realizar Nova compra";
		cout <<"\n2 - Remover produto do carrinho";
		cout <<"\n3 - Acessar carrinho de compras";
		cout <<"\n4 - Voltar ao Gerenciador de cadastro\n\n";
		cout << "---------------------------------------";
		cout << "\nValor total das compras feitas: " << Compra::total;
		cout << "\nQuantidade de produtos comprados: " << Compra::carrinho;
		cout << "\n---------------------------------------\n\n";
		int esc;

		escolha_menu(esc, 1, 4);

		ClearScreen();

		if(esc == 1){
			l.compra();

		}else if(esc == 2){
			l.remove_compra();

		}else if(esc == 3){
			ClearScreen();
			cout << "\n=============Nota fiscal=============\n\n";
			l.lista_compras();
			cout << "\n=====================================";
			cout << "\n\nVALOR TOTAL: " << Compra::total;
			cout << "\n\nQND. TOTAL DE ITENS: " << Compra::carrinho;
			cout << "\n=====================================\n\n";
			
		}else{
			pare = 0;
		}
	}

}


/** 
* @brief	Função que controla o menu principal do programa e chama
*			as operações escolhidas pelo usuário.
* @param	l lista principal do programa.
*/
void acessar_lista_p(Lista &l){
	int escolha;
	ClearScreen();

	int pare;
	pare = 1;

	while(pare == 1){

		cout << "\n---------Gerenciador de cadastros de produtos---------\n";
		cout << "\n1 - Cadastar novo produto";
		cout << "\n2 - Estoque (N° de produtos: " << Produto::lotes << ')';
		cout << "\n3 - Consultar produto ou setor de produtos";
		cout << "\n4 - Remover produto";
		cout << "\n5 - Alterar produto";
		cout << "\n6 - Menu compras";
		cout << "\n7 - Sair\n\n";


		escolha = 0;
		escolha_menu(escolha, 1, 8);

		ClearScreen();
		switch(escolha){
			case 1:
				cadastrar(l);
				break;

			case 2:
				l.listar();
				break;

			case 3:
				acessar_produto(l, escolha);
				break;

			case 4:
				acessar_produto(l, escolha);
				break;

			case 5:
				acessar_produto(l, escolha);
				break;

			case 6:
				menu_compras(l);
				break;

			case 7:
				pare = 0;
				break;

			default:
				break;
		}
	}
}