PROG = ./bin/Qlevetudo

BIN_DIR = ./bin
OBJ_DIR = ./build
DATA_DIR = ./data
LIB_DIR = ./lib
INC_DIR = ./include
SRC_DIR = ./src
DOC_DIR = ./doc
TEST_DIR = ./test

CC = g++

CPPFLAGS = -Wall -pedantic -ansi -std=c++11 -I. -I$(INC_DIR)/

OBJS = ./build/main.o ./build/lista.o ./build/bebidas.o ./build/frutas.o ./build/menu.o ./build/produtos.o ./build/perecivel.o ./build/cds.o ./build/livros.o ./build/salgados.o ./build/doces.o ./build/dvds.o ./build/registro.o ./build/compras.o ./build/util.o

RM = rm -rf

.PHONY: init all doxy doc clean go debug

debug: CPPFLAGS += -g -O0

$(PROG): $(OBJS)
	$(CC) $^ $(CPPFLAGS) -o $@

$(OBJ_DIR)/main.o: $(SRC_DIR)/main.cpp $(INC_DIR)/menu.h $(INC_DIR)/lista.h $(INC_DIR)/registro.h $(INC_DIR)/util.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/lista.o: $(SRC_DIR)/lista.cpp $(INC_DIR)/lista.h $(INC_DIR)/util.h $(INC_DIR)/em_comum.h 
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/bebidas.o: $(SRC_DIR)/bebidas.cpp $(INC_DIR)/bebidas.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/frutas.o: $(SRC_DIR)/frutas.cpp $(INC_DIR)/frutas.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/menu.o: $(SRC_DIR)/menu.cpp $(INC_DIR)/menu.h $(INC_DIR)/util.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/produtos.o: $(SRC_DIR)/produtos.cpp $(INC_DIR)/produtos.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/perecivel.o: $(SRC_DIR)/perecivel.cpp $(INC_DIR)/perecivel.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/cds.o: $(SRC_DIR)/cds.cpp $(INC_DIR)/cds.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/livros.o: $(SRC_DIR)/livros.cpp $(INC_DIR)/livros.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/salgados.o: $(SRC_DIR)/salgados.cpp $(INC_DIR)/salgados.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/doces.o: $(SRC_DIR)/doces.cpp $(INC_DIR)/doces.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/dvds.o: $(SRC_DIR)/dvds.cpp $(INC_DIR)/dvds.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/registro.o: $(SRC_DIR)/registro.cpp $(INC_DIR)/registro.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/compras.o: $(SRC_DIR)/compras.cpp $(INC_DIR)/compras.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/util.o: $(SRC_DIR)/util.cpp $(INC_DIR)/util.h
	$(CC) -c $(CPPFLAGS) -o $@ $<
	
dir:
	mkdir -p $(BIN_DIR)
	mkdir -p $(OBJ_DIR)
	mkdir -p $(DATA_DIR)
	mkdir -p $(INC_DIR)
	mkdir -p $(SRC_DIR)
	mkdir -p $(DOC_DIR)
	mkdir -p $(LIB_DIR)
	mkdir -p $(TEST_DIR)

doxy:
	$(RM) $(DOC_DIR)/*
	doxygen Doxyfile

clean:
	$(RM) $(OBJ_DIR)/*
	$(RM) $(BIN_DIR)/*

go:
	$(BIN_DIR)/Qlevetudo